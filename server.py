from flask import request, jsonify
from logic import validate_event, create_event
from db import Database

db = Database()

def start_server(app):
    @app.route('/api/v1/calendar/', methods=['POST'])
    def add_event():
        event_data = request.data.decode('utf-8').split('|')
        if validate_event(event_data):
            event = create_event(event_data)
            db.add_event(event)
            return jsonify({'message': 'Event added successfully'})
        else:
            return jsonify({'error': 'Invalid event data'})

    @app.route('/api/v1/calendar/')
    def get_events():
        events = db.get_events()
        return jsonify(events)

    @app.route('/api/v1/calendar/<int:event_id>/', methods=['GET', 'PUT', 'DELETE'])
    def event_by_id(event_id):
        if request.method == 'GET':
            event = db.get_event_by_id(event_id)
            if event:
                return jsonify(event)
            else:
                return jsonify({'error': 'Event not found'})

        if request.method == 'PUT':
            event_data = request.data.decode('utf-8').split('|')
            if validate_event(event_data):
                new_event = create_event(event_data)
                if db.update_event(event_id, new_event):
                    return jsonify({'message': 'Event updated successfully'})
                else:
                    return jsonify({'error': 'Event not found'})
            else:
                return jsonify({'error': 'Invalid event data'})

        if request.method == 'DELETE':
            if db.delete_event(event_id):
                return jsonify({'message': 'Event deleted successfully'})
            else:
                return jsonify({'error': 'Event not found'})