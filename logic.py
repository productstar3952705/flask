from model import Event

def add_event_to_calendar(calendar, event_data):
    event = create_event(event_data)
    calendar.add_event(event)

def update_event_in_calendar(calendar, event_id, new_event_data):
    if validate_event(new_event_data):
        new_event = create_event(new_event_data)
        if calendar.update_event(event_id, new_event):
            return "Событие успешно обновлено."
        else:
            return "Событие с указанным ID не найдено."
    else:
        return "Неверные данные события."

def delete_event_from_calendar(calendar, event_id):
    if calendar.delete_event(event_id):
        return "Событие успешно удалено."
    else:
        return "Событие с указанным ID не найдено."

def get_event_from_calendar(calendar, event_id):
    event = calendar.get_event_by_id(event_id)
    if event:
        return f"Событие найдено: {event.date}, {event.title}, {event.text}"
    else:
        return "Событие с указанным ID не найдено."