from flask import Flask, request, jsonify
from db import Database
from logic import add_event_to_calendar, update_event_in_calendar, delete_event_from_calendar, get_event_from_calendar

app = Flask(__name__)
db = Database()

@app.route('/add_event', methods=['POST'])
def add_event():
    event_data = request.json.get('event')
    if event_data:
        add_event_to_calendar(db, event_data)
        return jsonify({"message": "Событие успешно добавлено."})
    else:
        return jsonify({"message": "Неверный формат данных."}), 400

@app.route('/update_event/<int:event_id>', methods=['PUT'])
def update_event(event_id):
    new_event_data = request.json.get('event')
    if new_event_data:
        return jsonify({"message": update_event_in_calendar(db, event_id, new_event_data)})
    else:
        return jsonify({"message": "Неверный формат данных."}), 400

@app.route('/delete_event/<int:event_id>', methods=['DELETE'])
def delete_event(event_id):
    return jsonify({"message": delete_event_from_calendar(db, event_id)})

@app.route('/get_event/<int:event_id>', methods=['GET'])
def get_event(event_id):
    return jsonify({"message": get_event_from_calendar(db, event_id)})

def start_server(app):
    app.run(debug=True)

if __name__ == '__main__':
    start_server(app)