class Database:
    def __init__(self):
        self.events = []

    def add_event(self, event):
        self.events.append(event)

    def get_events(self):
        return self.events

    def get_event_by_id(self, event_id):
        for event in self.events:
            if event['ID'] == event_id:
                return event
        return None

    def update_event(self, event_id, new_event):
        for i, event in enumerate(self.events):
            if event['ID'] == event_id:
                self.events[i] = new_event
                return True
        return False

    def delete_event(self, event_id):
        for i, event in enumerate(self.events):
            if event['ID'] == event_id:
                del self.events[i]
                return True
        return False